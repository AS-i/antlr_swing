package tb.antlr.symbolTable;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;

public class LocalSymbols {
	
	Deque<HashMap<String, Integer>> memory = new ArrayDeque<>();

	public LocalSymbols() {
		memory.push(new HashMap<>()); //the global scope
	}
	
	public Integer enterScope() {
		memory.push(new HashMap<>());
		return memory.size();
	}
	
	public Integer leaveScope() throws RuntimeException {
		if (memory.size()<=1)
			throw new RuntimeException("Cannot leave the global scope!");
		memory.remove();
		return memory.size();
	}
	
	public boolean hasSymbol(String name) {
		return memory.peek().containsKey(name);
	}

	public HashMap<String, Integer> hasSymbolDepth(String name) {
		Iterator<HashMap<String, Integer>> it = memory.iterator();
		while(it.hasNext())
		{
			HashMap<String, Integer> symTab = it.next();
			if (symTab.containsKey(name))
				return symTab;
		}
		return null;
	}

	public void newSymbol(String name) throws RuntimeException{
		if (! hasSymbol(name))
			memory.peek().put(name, null);
		else
			throw new RuntimeException("Variable " + name +" exists in the same scope!");
	}

	public Integer setSymbol(String name, Integer value) throws RuntimeException {
		HashMap<String, Integer> symTab = hasSymbolDepth(name);
		if(symTab != null) {
			symTab.put(name, value);
			return value;
		}
		else
			throw new RuntimeException("Variable " + name +" does not exist!");
	}

	public Integer getSymbol(String name) throws RuntimeException {
		HashMap<String, Integer> symTab = hasSymbolDepth(name);
		if(symTab != null) {
			return symTab.get(name);
		}
		else
			throw new RuntimeException("Variable " + name +" does not exist!");
	}
	
	
}
