tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer for_c = 0;
  
}
prog    : (e+=expr | d+=decl| e+=loops )* -> template(name={$e},deklaracje={$d}) 
<<<deklaracje> 
start:
<name>
>>;

block   : LB (e+=expr | e+=loops)* {for_c++;} RB -> template(name={$e}) 
<<
<name>
>>;

loops : (dw+=do_while| dw+=for_f | dw+=while_w)  -> template(dw={$dw})"<dw>";

while_w : l_cond=dw_cond bb=block-> petla_while(for_c={for_c.toString()},body={$bb.st}, cond={$l_cond.st});

do_while :  DO bb=block l_cond=dw_cond -> petla_do_while(for_c={for_c.toString()},body={$bb.st}, cond={$l_cond.st});

dw_cond   : WHILE LP c=cond RP -> template(p1={$c.st}) "<p1>";

for_f  :  (FOR init=for_init l_cond=for_cond incr=for_incr bb=block) -> petla_for(for_c={for_c.toString()},body={$bb.st}, cond={$l_cond.st}, init={$init.st}, incr={$incr.st});

for_init    :   LP e=expr SEMI-> template(instr={$e.st})"<instr>"; 

for_cond    :   c=cond SEMI -> template(p={$c.st}) "<p>";

for_incr    :   e=expr RP-> template(instr={$e.st})"<instr>"; 

cond  : znak=equ_signs -> template(znak={$znak.st})"<znak>";

equ_signs  : ^(MORE e1=expr e2=expr) ->more(p1={$e1.st},p2={$e2.st})
           |^(LESS e1=expr e2=expr) ->less(p1={$e1.st},p2={$e2.st})
           |^(MORE_E e1=expr e2=expr) ->more_e(p1={$e1.st},p2={$e2.st})
           |^(LESS_E e1=expr e2=expr) ->less_e(p1={$e1.st},p2={$e2.st})
           |^(EQU e1=expr e2=expr) ->equ(p1={$e1.st},p2={$e2.st})
           |^(NEQU e1=expr e2=expr) ->nequ(p1={$e1.st},p2={$e2.st});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text}) 
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
 
expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST id=ID   e2=expr) -> podstaw(p1={$id.text},p2={$e2.st})
        | ID                       -> ref(name={$ID.text})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()});
    