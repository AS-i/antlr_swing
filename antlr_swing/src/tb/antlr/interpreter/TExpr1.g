tree grammar TExpr1;


options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}



prog
    :
    (block | globalDecl | gS=globalSet {System.out.println("GlobalSet: "+$gS.out);})*;
    
globalDecl
:  
  ^(VAR id=ID) {globalDeclare($id.text);}   //create Int variable named id
;

globalSet returns [Integer out]
:
  ^(PODST id=ID   e2=expr) {globalSet($id.text, $e2.out); $out = globalRefer($id.text);}|
    ID                       {$out = globalRefer($ID.text);   }|
    INT                      {$out = getInt($INT.text);}
;

block
      : 
      {System.out.println("BLOCK:");}
      LB {blockIn();} 
        (e=expr {System.out.println($e.out);} | localDecl | block)* //(e= expr{System.out.println($e.out);} | d= blockDecl | block {newBlock();}})*
      RB {blockOut();}
      ;
        
 

expr returns [Integer out]
:
		^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;} |
		^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;} |
		^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;} |
		^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;} |
		^(PODST id=ID   e2=expr) {localSet($id.text, $e2.out); $out = localRefer($id.text);}|
		ID                       {$out = localRefer($ID.text);   }|
		INT                      {$out = getInt($INT.text);}
;

localDecl:
  ^(VAR id=ID) {localDeclare($id.text);}
;



