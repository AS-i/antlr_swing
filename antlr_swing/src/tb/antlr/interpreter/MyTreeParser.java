package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols.*;
import tb.antlr.symbolTable.LocalSymbols.*;
public class MyTreeParser extends TreeParser {
	tb.antlr.symbolTable.GlobalSymbols globals = new tb.antlr.symbolTable.GlobalSymbols();
	tb.antlr.symbolTable.LocalSymbols locals = new tb.antlr.symbolTable.LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void globalDeclare(String text)
	{
		globals.newSymbol(text);
	}
	
	protected void globalSet(String text, Integer val)
	{
		globals.setSymbol(text, val);
	}
	
	protected Integer globalRefer(String text)
	{
		return globals.getSymbol(text);
	}
	
	protected void blockIn()
	{
		locals.enterScope();// automatic
	}
	protected void blockOut()
	{
		locals.leaveScope();// automatic
	}
	
	protected void localDeclare(String text)
	{
		locals.newSymbol(text);
	} 
	
	protected void localSet(String text, Integer val)
	{
		try { locals.setSymbol(text, val);}
		catch(RuntimeException e)
		{
			globals.setSymbol(text, val);
		}

	}
	
	protected Integer localRefer(String text)  throws RuntimeException 
	{
		//if not get from block up
		Integer val;
		try { val= locals.getSymbol(text);}
		catch(RuntimeException e)
		{
			val=globals.getSymbol(text);
		}
		return val;
	}
}
