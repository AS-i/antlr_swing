grammar Expr;

options {
  output=AST;
  backtrack=true;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    | VAR ID NL -> ^(VAR ID) 
    | ID PODST expr NL -> ^(PODST ID expr)
    | NL ->
    |for_
    |do_while
    |while_
    ;

expr
    : addExpr
      ( MORE^ addExpr
      | LESS^ addExpr
      | MORE_E^ addExpr
      | LESS_E^ addExpr
      | EQU^ addExpr
      | NEQU^ addExpr
      )*
    ;
      
      
addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom 
      | DIV^ atom
      )*
    ;


atom
    : INT
    | ID
    | LP! expr RP!
    ;


while_
  :
    WHILE LP dw_cond RP block
   ;

do_while
  : 
     DO block WHILE LP dw_cond RP
  ;
  
dw_cond
  :
  expr
  ;

for_
  :
    FOR for_init for_cond for_incr block
  ;

for_cond
  :
  dw_cond SEMI
  ;
  
for_init
  :
  LP stat SEMI 
  ;
  
for_incr
	:
	stat RP
	;

block
  :
    LB (stat)* RB 
  ;

VAR :'var';

FOR
  : 'for'
  ;

DO
  : 'do'
  ;
  
WHILE
   :  'while'
   ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


SEMI
  :  ';'
  ;

MORE
  : '>'
  ;
  
MORE_E
  : '>='
  ;

LESS
  : '<'
  ;


LESS_E
  : '<='
  ;
  
EQU
  : '=='
  ;
  
NEQU
  : '!='
  ;

LB
  : '{'
  ;
  
RB
  : '}'
  ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
